/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : web

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-12-27 22:42:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for web
-- ----------------------------
DROP TABLE IF EXISTS `web`;
CREATE TABLE `web` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `webname` varchar(32) NOT NULL,
  `weblink` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web
-- ----------------------------
INSERT INTO `web` VALUES ('3', '天猫', 'https://www.tmall.com/');
INSERT INTO `web` VALUES ('4', 'A站', 'http://www.acfun.tv/');
INSERT INTO `web` VALUES ('5', 'B站', 'http://www.bilibili.com/');
INSERT INTO `web` VALUES ('6', '战网', 'http://www.battlenet.com.cn/');
INSERT INTO `web` VALUES ('7', '红岩网校', 'http://202.202.43.125/');
INSERT INTO `web` VALUES ('8', 'BT Down', 'http://172.22.161.11/');
INSERT INTO `web` VALUES ('13', '重邮', 'http://www.cqupt.edu.cn/');
INSERT INTO `web` VALUES ('14', '百度', 'http：//www.baidu.com');
